# run_orthomcl

script to do Orthomcl (http://www.orthomcl.org/orthomcl/)

Note that Orthomcl needs to be installed in advance. Please see the webpage of Orthomcl above to get the information of the installation.


References:

Li L, Stoeckert CJ Jr, Roos DS: OrthoMCL: identification of ortholog groups for eukaryotic genomes. Genome Res 2003, 13:2178-2189.
